* .NET Code MVC Application to test a TaxCalculator
* TaxJar calculator calls the API methods
* Implemented a factory to dynamically get an specific TaxCalculator
* The caller will decide which TaxCalculator it should use depending on a parameter
* Injected Services, Factories, TaxCalculators, etc using Dependency Injection, provided by the framework
* Included a UI to test the TaxService
* Added an additional FlatTaxCalculator to test my solution with different TaxCalculator and totest the factory
