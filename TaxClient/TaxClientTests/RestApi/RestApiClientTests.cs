using FluentAssertions;
using NUnit.Framework;
using RichardSzalay.MockHttp;
using System.Collections.Generic;
using System.Net.Http;
using TaxClient.RestApi;

namespace TaxClientTests.RestApi
{
    public class RestApiClientTests
    {
        private readonly RestApiClient _restApiClient;
        private readonly MockHttpMessageHandler _mockHttp;

        public RestApiClientTests()
        {
            _mockHttp = new MockHttpMessageHandler();
            var httpClient = new HttpClient(_mockHttp);

            _restApiClient = new RestApiClient(httpClient);
        }

        [SetUp]
        public void Setup()
        {
        }

        [TearDown]
        public void TearDown()
        {
            _mockHttp.VerifyNoOutstandingExpectation();
            _mockHttp.VerifyNoOutstandingRequest();
        }

        [Test]
        [TestCase("ABC123", TestName = "Get: With AccessToken")]
        [TestCase("", TestName = "Get: Without AccessToken")]
        public void Get(string accessToken)
        {
            const string resultData = @"
            {
                ""Value1"": ""Test 1"",
                ""Value2"": ""Test 2""
            }";
            const string url = "https://api.url.com";

            var headersDictionary = new Dictionary<string, string>();
            if(!string.IsNullOrWhiteSpace(accessToken))
                headersDictionary.Add("Authorization", "Bearer ABC123");

            _mockHttp.When(HttpMethod.Get, url)
                .WithHeaders(headersDictionary)
                .Respond("application/json", resultData);

            var result = _restApiClient.GetAsync<DummyResponse>(url, accessToken).GetAwaiter().GetResult();

            result.Should().BeEquivalentTo(new
            {
                Value1 = "Test 1",
                Value2 = "Test 2"
            });
        }

        [Test]
        [TestCase("ABC123", TestName = "Post: With AccessToken")]
        [TestCase("", TestName = "Post: Without AccessToken")]
        public void Post(string accessToken)
        {
            const string resultData = @"
            {
                ""Value1"": ""Test 1"",
                ""Value2"": ""Test 2""
            }";
            const string url = "https://api.url.com";
            var request = new DummyRequest
            {
                ValueA = "Test A",
                ValueB = "Test B"
            };

            var headersDictionary = new Dictionary<string, string>();
            if (!string.IsNullOrWhiteSpace(accessToken))
                headersDictionary.Add("Authorization", "Bearer ABC123");
            _mockHttp.When(HttpMethod.Post, url)
                .WithHeaders(headersDictionary)
                .Respond("application/json", resultData);

            var result = _restApiClient.PostAsync<DummyResponse, DummyRequest>(url, request, accessToken).GetAwaiter().GetResult();

            result.Should().BeEquivalentTo(new
            {
                Value1 = "Test 1",
                Value2 = "Test 2"
            });
        }
    }

    public class DummyRequest
    {
        public string ValueA { get; set; }
        public string ValueB { get; set; }
    }

    public class DummyResponse
    {
        public string Value1 { get; set; }
        public string Value2 { get; set; }
    }
}