using FluentAssertions;
using NUnit.Framework;
using TaxClient.Extensions;
using TaxClient.ResourceModels;

namespace TaxClientTests.Extensions
{
    public class CalculateSalesTaxRequestViewModelTests
    {
        [Test]
        [TestCase("TestCity", "TestCounty", "TestState", "TestStreet", "city=TestCity&county=TestCounty&state=TestState&street=TestStreet", TestName = "BuildQueryString: All values")]
        [TestCase("", "TestCounty", "TestState", "TestStreet", "county=TestCounty&state=TestState&street=TestStreet", TestName = "BuildQueryString: All values but city")]
        [TestCase("TestCity", "", "TestState", "TestStreet", "city=TestCity&state=TestState&street=TestStreet", TestName = "BuildQueryString: All values but county")]
        [TestCase("TestCity", "TestCounty", "", "TestStreet", "city=TestCity&county=TestCounty&street=TestStreet", TestName = "BuildQueryString: All values but state")]
        [TestCase("TestCity", "TestCounty", "TestState", "", "city=TestCity&county=TestCounty&state=TestState", TestName = "BuildQueryString: All values but street")]
        [TestCase("", "", "", "", "", TestName = "BuildQueryString: All blank values")]
        [TestCase(null, null, null, null, "", TestName = "BuildQueryString: All null values")]
        public void BuildQueryString(string city, string county, string state, string street, string expectedResult)
        {
            var model = new GetTaxRatesRequestModel
            {
                City = city,
                County = county,
                State = state,
                Street = street
            };

            var result = model.BuildQueryString();

            result.Should().Be(expectedResult);
        }
    }
}