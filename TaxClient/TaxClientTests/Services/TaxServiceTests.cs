using FluentAssertions;
using Moq;
using NUnit.Framework;
using TaxClient.Enums;
using TaxClient.ResourceModels;
using TaxClient.Services;
using TaxClient.TaxCalculators;

namespace TaxClientTests.Services
{
    public class TaxServiceTests
    {
        private readonly TaxService _taxService;
        private readonly Mock<ITaxCalculatorFactory> _taxCalculatorFactory;
        private readonly Mock<ITaxCalculator> _taxCalculator;

        public TaxServiceTests()
        {
            _taxCalculator = new Mock<ITaxCalculator>(MockBehavior.Strict);
            _taxCalculatorFactory = new Mock<ITaxCalculatorFactory>(MockBehavior.Strict);

            _taxService = new TaxService(_taxCalculatorFactory.Object);
        }

        [SetUp]
        public void Setup()
        {
            _taxCalculatorFactory.Setup(x => x.GetTaxCalculator(TaxCalculatorType.TaxJar)).Returns(_taxCalculator.Object);
        }

        [TearDown]
        public void TearDown()
        {
            Mock.VerifyAll(_taxCalculator, _taxCalculatorFactory);
        }

        [Test]
        public void CalculateSalesTaxes()
        {
            var expectedResult = new CalculateSalesTaxResponseModel();
            _taxCalculator.Setup(x => x.CalculateSalesTax(It.IsAny<CalculateSalesTaxRequestModel>())).Returns(expectedResult);

            var model = new CalculateSalesTaxRequestModel();
            var result = _taxService.CalculateSalesTaxes(model, TaxCalculatorType.TaxJar);

            result.Should().Be(expectedResult);
        }

        [Test]
        public void GetTaxRates()
        {
            var expectedResult = new GetTaxRatesResponseModel();
            _taxCalculator.Setup(x => x.GetTaxRates(It.IsAny<GetTaxRatesRequestModel>())).Returns(expectedResult);

            var model = new GetTaxRatesRequestModel();
            var result = _taxService.GetTaxRates(model, TaxCalculatorType.TaxJar);

            result.Should().Be(expectedResult);
        }
    }
}