using AutoFixture;
using AutoMapper;
using FluentAssertions;
using NUnit.Framework;
using TaxClient.Models;
using TaxClient.ResourceModels;

namespace TaxClientTests.Models
{
    public class GetTaxRatesRequestViewModelTests
    {
        private readonly IFixture _fixture;
        private readonly IMapper _mapper;

        public GetTaxRatesRequestViewModelTests()
        {
            _fixture = new Fixture();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<GetTaxRatesRequestViewModel.AutoMapperProfile>();
            });
            _mapper = config.CreateMapper();
        }

        [Test]
        public void Map()
        {
            var viewModel = _fixture.Create<GetTaxRatesRequestViewModel>();

            var result = _mapper.Map<GetTaxRatesRequestModel>(viewModel);

            result.Should().BeEquivalentTo(viewModel, opt => opt.Excluding(x => x.TaxCalculatorType));
        }
    }
}