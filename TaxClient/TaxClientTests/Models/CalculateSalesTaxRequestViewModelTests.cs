using AutoFixture;
using AutoMapper;
using FluentAssertions;
using NUnit.Framework;
using TaxClient.Models;
using TaxClient.ResourceModels;

namespace TaxClientTests.Models
{
    public class CalculateSalesTaxRequestViewModelTests
    {
        private readonly IFixture _fixture;
        private readonly IMapper _mapper;

        public CalculateSalesTaxRequestViewModelTests()
        {
            _fixture = new Fixture();

            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CalculateSalesTaxRequestViewModel.AutoMapperProfile>();
            });
            _mapper = config.CreateMapper();
        }

        [Test]
        public void Map()
        {
            var viewModel = _fixture.Create<CalculateSalesTaxRequestViewModel>();

            var result = _mapper.Map<CalculateSalesTaxRequestModel>(viewModel);

            result.Should().BeEquivalentTo(viewModel, opt => opt.Excluding(x => x.TaxCalculatorType));
        }
    }
}