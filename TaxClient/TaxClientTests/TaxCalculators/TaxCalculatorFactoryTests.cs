using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using TaxClient.Enums;
using TaxClient.TaxCalculators;

namespace TaxClientTests.TaxCalculators
{
    public class TaxCalculatorFactoryTests
    {
        private readonly Mock<IServiceProvider> _serviceProvider;
        private readonly TaxCalculatorFactory _taxCalculatorFactory;

        public TaxCalculatorFactoryTests()
        {
            _serviceProvider = new Mock<IServiceProvider>(MockBehavior.Strict);

            _taxCalculatorFactory = new TaxCalculatorFactory(_serviceProvider.Object);
        }

        [SetUp]
        public void Setup()
        {
        }

        [TearDown]
        public void TearDown()
        {
            Mock.VerifyAll(_serviceProvider);
        }

        [Test]
        [TestCase(TaxCalculatorType.Flat, typeof(FlatTaxCalculator), TestName = "Flat")]
        [TestCase(TaxCalculatorType.TaxJar, typeof(TaxJarCalculator), TestName = "TaxJar")]
        public void GetTaxCalculator(TaxCalculatorType taxCalculatorType, Type calculatorType)
        {
            var args = taxCalculatorType == TaxCalculatorType.Flat ? Array.Empty<object>() : new object[] {null, null};
            _serviceProvider.Setup(x => x.GetService(calculatorType)).Returns(Activator.CreateInstance(calculatorType, args));

            var result = _taxCalculatorFactory.GetTaxCalculator(taxCalculatorType);

            result.GetType().Should().Be(calculatorType);
        }
    }
}