using FluentAssertions;
using Microsoft.Extensions.Configuration;
using Moq;
using NUnit.Framework;
using TaxClient.ResourceModels;
using TaxClient.RestApi;
using TaxClient.TaxCalculators;

namespace TaxClientTests.TaxCalculators
{
    public class TaxJarCalculatorTests
    {
        private const string AccessToken = "ABCD1234";
        private const string BaseUrl = "https://api.tax.com/";
        
        private readonly Mock<IConfiguration> _configuration;
        private readonly Mock<IRestApiClient> _restApiClient;
        private readonly TaxJarCalculator _taxJarCalculator;

        public TaxJarCalculatorTests()
        {
            _configuration = new Mock<IConfiguration>(MockBehavior.Strict);
            _restApiClient = new Mock<IRestApiClient>(MockBehavior.Strict);

            _taxJarCalculator = new TaxJarCalculator(_restApiClient.Object, _configuration.Object);
        }

        [SetUp]
        public void Setup()
        {
            _configuration.Setup(x => x["TaxJar-Api-AccessToken"]).Returns(AccessToken);
            _configuration.Setup(x => x["TaxJar-Api-BaseUrl"]).Returns(BaseUrl);
        }

        [TearDown]
        public void TearDown()
        {
            Mock.VerifyAll(_configuration, _restApiClient);
        }

        [Test]
        public void CalculateSalesTax()
        {
            var expectedResult = new CalculateSalesTaxResponseModel();
            _restApiClient
                .Setup(x => x.PostAsync<CalculateSalesTaxResponseModel, CalculateSalesTaxRequestModel>($"{BaseUrl}taxes", It.IsAny<CalculateSalesTaxRequestModel>(), AccessToken))
                .ReturnsAsync(expectedResult);

            var requestModel = new CalculateSalesTaxRequestModel();
            var result = _taxJarCalculator.CalculateSalesTax(requestModel);

            result.Should().Be(expectedResult);
        }

        [Test]
        public void GetTaxRates()
        {
            const string zip = "12345";
            var expectedResult = new GetTaxRatesResponseModel();
            _restApiClient
                .Setup(x => x.GetAsync<GetTaxRatesResponseModel>($"{BaseUrl}rates/{zip}?", AccessToken))
                .ReturnsAsync(expectedResult);

            var requestModel = new GetTaxRatesRequestModel
            {
                Zip = zip
            };
            var result = _taxJarCalculator.GetTaxRates(requestModel);

            result.Should().Be(expectedResult);
        }
    }
}