using AutoMapper;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using TaxClient.Controllers;
using TaxClient.Enums;
using TaxClient.Models;
using TaxClient.ResourceModels;
using TaxClient.Services;

namespace TaxClientTests.Controllers
{
    public class HomeControllerTests
    {
        private readonly HomeController _homeController;
        private readonly IMapper _mapper;
        private readonly Mock<ITaxService> _taxService;

        public HomeControllerTests()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<CalculateSalesTaxRequestViewModel.AutoMapperProfile>();
                cfg.AddProfile<GetTaxRatesRequestViewModel.AutoMapperProfile>();
            });
            _mapper = config.CreateMapper();
            _taxService = new Mock<ITaxService>(MockBehavior.Strict);

            _homeController = new HomeController(_mapper, _taxService.Object);
        }

        [SetUp]
        public void Setup()
        {
        }

        [TearDown]
        public void TearDown()
        {
            Mock.VerifyAll(_taxService);
        }

        [Test]
        public void CalculateTaxes()
        {
            _taxService.Setup(x => x.CalculateSalesTaxes(It.IsAny<CalculateSalesTaxRequestModel>(), TaxCalculatorType.TaxJar)).Returns(new CalculateSalesTaxResponseModel());

            var viewModel = new CalculateSalesTaxRequestViewModel {TaxCalculatorType = TaxCalculatorType.TaxJar};
            var result = _homeController.CalculateTaxes(viewModel) as OkObjectResult;

            result?.Value.Should().NotBeNull();
        }

        [Test]
        public void GetTaxRates()
        {
            _taxService.Setup(x => x.GetTaxRates(It.IsAny<GetTaxRatesRequestModel>(), TaxCalculatorType.TaxJar)).Returns(new GetTaxRatesResponseModel());

            var viewModel = new GetTaxRatesRequestViewModel {TaxCalculatorType = TaxCalculatorType.TaxJar};
            var result = _homeController.GetTaxRates(viewModel) as OkObjectResult;

            result?.Value.Should().NotBeNull();
        }
    }
}