﻿using AutoMapper;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using TaxClient.Enums;
using TaxClient.ResourceModels;

[assembly: InternalsVisibleTo("TaxClientTests")]
namespace TaxClient.Models
{
    public class GetTaxRatesRequestViewModel
    {
        public string City { get; set; }
        public string County { get; set; }
        public string State { get; set; }
        public string Street { get; set; }

        [Display(Name = "Tax Calculator Type")]
        public TaxCalculatorType TaxCalculatorType { get; set; } = TaxCalculatorType.TaxJar;

        [Required]
        public string Zip { get; set; }

        internal class AutoMapperProfile : Profile
        {
            public AutoMapperProfile()
            {
                CreateMap<GetTaxRatesRequestViewModel, GetTaxRatesRequestModel>();
            }
        }
    }
}
