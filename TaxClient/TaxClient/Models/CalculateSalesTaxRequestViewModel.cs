﻿using AutoMapper;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using TaxClient.Enums;
using TaxClient.ResourceModels;

[assembly: InternalsVisibleTo("TaxClientTests")]
namespace TaxClient.Models
{
    public class CalculateSalesTaxRequestViewModel
    {
        public double Amount { get; set; }

        [Display(Name = "Form City")]
        public string FromCity { get; set; }

        [Display(Name = "Form Country")]
        public string FromCountry { get; set; }

        [Display(Name = "Form State")]
        public string FromState { get; set; }

        [Display(Name = "Form Street")]
        public string FromStreet { get; set; }

        [Display(Name = "Form Zip")]
        public string FromZip { get; set; }

        [Required]
        public double Shipping { get; set; }

        [Display(Name = "Tax Calculator Type")]
        public TaxCalculatorType TaxCalculatorType { get; set; } = TaxCalculatorType.TaxJar;

        [Display(Name = "To City")]
        public string ToCity { get; set; }

        [Display(Name = "To Country")]
        [Required]
        public string ToCountry { get; set; }

        [Display(Name = "To State")]
        [Required]
        public string ToState { get; set; }

        [Display(Name = "To Street")]
        public string ToStreet { get; set; }

        [Display(Name = "To Zip")]
        [Required]
        public string ToZip { get; set; }

        internal class AutoMapperProfile : Profile
        {
            public AutoMapperProfile()
            {
                CreateMap<CalculateSalesTaxRequestViewModel, CalculateSalesTaxRequestModel>();
            }
        }
    }
}
