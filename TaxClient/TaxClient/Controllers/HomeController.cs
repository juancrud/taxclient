﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TaxClient.Enums;
using TaxClient.Models;
using TaxClient.ResourceModels;
using TaxClient.Services;

namespace TaxClient.Controllers
{
    public class HomeController : Controller
    {
        private readonly ITaxService _taxService;
        private readonly IMapper _mapper;
        
        public HomeController(IMapper mapper, ITaxService taxService)
        {
            _mapper = mapper;
            _taxService = taxService;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult GetTaxRates(GetTaxRatesRequestViewModel viewModel)
        {
            var resourceModel = _mapper.Map<GetTaxRatesRequestModel>(viewModel);
            var result = _taxService.GetTaxRates(resourceModel, viewModel.TaxCalculatorType);
            return Ok(JsonConvert.SerializeObject(result, Formatting.Indented));
        }

        public IActionResult CalculateTaxes(CalculateSalesTaxRequestViewModel viewModel)
        {
            var resourceModel = _mapper.Map<CalculateSalesTaxRequestModel>(viewModel);
            var result = _taxService.CalculateSalesTaxes(resourceModel, viewModel.TaxCalculatorType);
            return Ok(JsonConvert.SerializeObject(result, Formatting.Indented));
        }
    }
}
