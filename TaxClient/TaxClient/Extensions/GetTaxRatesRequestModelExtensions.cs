﻿using System.Collections.Generic;
using TaxClient.ResourceModels;

namespace TaxClient.Extensions
{
    public static class GetTaxRatesRequestModelExtensions
    {
        public static string BuildQueryString(this GetTaxRatesRequestModel requestModel)
        {
            var parameters = new List<string>();
            if (!string.IsNullOrWhiteSpace(requestModel.City))
            {
                parameters.Add($"city={requestModel.City}");
            }
            if (!string.IsNullOrWhiteSpace(requestModel.County))
            {
                parameters.Add($"county={requestModel.County}");
            }
            if (!string.IsNullOrWhiteSpace(requestModel.State))
            {
                parameters.Add($"state={requestModel.State}");
            }
            if (!string.IsNullOrWhiteSpace(requestModel.Street))
            {
                parameters.Add($"street={requestModel.Street}");
            }

            return string.Join("&", parameters);
        }
    }
}
