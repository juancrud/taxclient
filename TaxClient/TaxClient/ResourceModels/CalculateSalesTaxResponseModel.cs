﻿using Newtonsoft.Json;

namespace TaxClient.ResourceModels
{
    public class CalculateSalesTaxResponseModel
    {
        [JsonProperty("tax")]
        public TaxResponseModel TaxResponseModel { get; set; }
    }

    public class TaxResponseModel
    {
        [JsonProperty("amount_to_collect")]
        public double AmountToCollect { get; set; }

        [JsonProperty("breakdown")]
        public BreakdownResponseModel Breakdown { get; set; }

        [JsonProperty("exemption_type")]
        public string ExemptionType { get; set; }

        [JsonProperty("freight_taxable")]
        public bool FreightTaxable { get; set; }

        [JsonProperty("has_nexus")]
        public bool HasNexus { get; set; }

        [JsonProperty("jurisdictions")]
        public JurisdictionsResponseModel Jurisdictions { get; set; }

        [JsonProperty("order_total_amount")]
        public double OrderTotalAmount { get; set; }

        [JsonProperty("rate")]
        public double Rate { get; set; }

        [JsonProperty("shipping")]
        public double Shipping { get; set; }

        [JsonProperty("taxable_amount")]
        public double TaxableAmount { get; set; }

        [JsonProperty("tax_source")]
        public string TaxSource { get; set; }
    }

    public class BreakdownResponseModel
    {
        [JsonProperty("city_taxable_amount")]
        public double CityTaxableAmount { get; set; }

        [JsonProperty("city_tax_collectable")]
        public double CityTaxCollectable { get; set; }

        [JsonProperty("city_tax_rate")]
        public double CityTaxRate { get; set; }

        [JsonProperty("combined_tax_rate")]
        public double CombinedTaxRate { get; set; }

        [JsonProperty("county_taxable_amount")]
        public double CountyTaxableAmount { get; set; }

        [JsonProperty("county_tax_collectable")]
        public double CountyTaxCollectable { get; set; }

        [JsonProperty("county_tax_rate")]
        public double CountyTaxRate { get; set; }

        [JsonProperty("line_items")]
        public object LineItems { get; set; }

        [JsonProperty("shipping")]
        public object Shipping { get; set; }

        [JsonProperty("special_district_taxable_amount")]
        public double SpecialDistrictTaxableAmount { get; set; }

        [JsonProperty("special_district_tax_collectable")]
        public double SpecialDistrictTaxCollectable { get; set; }

        [JsonProperty("special_tax_rate")]
        public double SpecialTaxRate { get; set; }

        [JsonProperty("state_taxable_amount")]
        public double StateTaxableAmount { get; set; }

        [JsonProperty("state_tax_collectable")]
        public double StateTaxCollectable { get; set; }

        [JsonProperty("state_tax_rate")]
        public double StateTaxRate { get; set; }

        [JsonProperty("tax_collectable")]
        public double TaxCollectable { get; set; }

        [JsonProperty("taxable_amount")]
        public double TaxableAmount { get; set; }
    }

    public class JurisdictionsResponseModel
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("county")]
        public string County { get; set; }
        
        [JsonProperty("state")]
        public string State { get; set; }
    }
}
