﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TaxClient.ResourceModels
{
    public class CalculateSalesTaxRequestModel
    {
        [JsonProperty("amount")]
        public double Amount { get; set; }

        [JsonProperty("customer_id")]
        public string CustomerId { get; set; } = "1";

        [JsonProperty("exemption_type")]
        public string ExemptionType { get; set; }

        [JsonProperty("from_city")]
        public string FromCity { get; set; }

        [JsonProperty("from_country")]
        public string FromCountry { get; set; }

        [JsonProperty("from_state")]
        public string FromState { get; set; }

        [JsonProperty("from_street")]
        public string FromStreet { get; set; }

        [JsonProperty("from_zip")]
        public string FromZip { get; set; }

        [JsonProperty("shipping")]
        public double Shipping { get; set; }

        [JsonProperty("to_city")]
        public string ToCity { get; set; }

        [JsonProperty("to_country")]
        public string ToCountry { get; set; }

        [JsonProperty("to_state")]
        public string ToState { get; set; }

        [JsonProperty("to_street")]
        public string ToStreet { get; set; }

        [JsonProperty("to_zip")]
        public string ToZip { get; set; }

        [JsonProperty("nexus_addresses")]
        public List<NexusAddressRequestModel> NexusAddresses { get; set; }

        [JsonProperty("line_items")]
        public List<LineItemRequestModel> LineItems { get; set; }
    }

    public class LineItemRequestModel
    {
        [JsonProperty("discount")]
        public double Discount { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("product_tax_code")]
        public string ProductTaxCode { get; set; }

        [JsonProperty("quantity")]
        public int Quantity { get; set; }

        [JsonProperty("unit_price")]
        public double UnitPrice { get; set; }
    }

    public class NexusAddressRequestModel
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }
    }
}
