﻿using Newtonsoft.Json;

namespace TaxClient.ResourceModels
{
    public class GetTaxRatesRequestModel
    {
        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("county")]
        public string County { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("street")]
        public string Street { get; set; }

        [JsonProperty("zip")]
        public string Zip { get; set; }
    }
}
