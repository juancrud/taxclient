﻿namespace TaxClient.Enums
{
    public enum TaxCalculatorType
    {
        TaxJar = 1,
        Flat = 2
    }
}
