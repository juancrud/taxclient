using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TaxClient.RestApi;
using TaxClient.Services;
using TaxClient.TaxCalculators;

namespace TaxClient
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddAutoMapper(typeof(Startup));

            services.AddHttpClient<IRestApiClient, RestApiClient>();
            services.AddScoped<ITaxService, TaxService>();
            services.AddScoped<ITaxCalculatorFactory, TaxCalculatorFactory>();
            services.AddScoped<TaxJarCalculator>().AddScoped<ITaxCalculator, TaxJarCalculator>(x => x.GetService<TaxJarCalculator>());
            services.AddScoped<FlatTaxCalculator>().AddScoped<ITaxCalculator, FlatTaxCalculator>(x => x.GetService<FlatTaxCalculator>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
