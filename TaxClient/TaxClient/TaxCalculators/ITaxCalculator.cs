﻿using TaxClient.ResourceModels;

namespace TaxClient.TaxCalculators
{
    public interface ITaxCalculator
    {
        CalculateSalesTaxResponseModel CalculateSalesTax(CalculateSalesTaxRequestModel requestModel);
        GetTaxRatesResponseModel GetTaxRates(GetTaxRatesRequestModel requestModel);
    }
}
