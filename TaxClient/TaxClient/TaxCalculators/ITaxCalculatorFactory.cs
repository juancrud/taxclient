﻿using TaxClient.Enums;

namespace TaxClient.TaxCalculators
{
    public interface ITaxCalculatorFactory
    {
        ITaxCalculator GetTaxCalculator(TaxCalculatorType taxCalculator);
    }
}
