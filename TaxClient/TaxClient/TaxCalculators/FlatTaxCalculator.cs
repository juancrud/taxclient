﻿using TaxClient.ResourceModels;

namespace TaxClient.TaxCalculators
{
    public class FlatTaxCalculator : ITaxCalculator
    {
        public CalculateSalesTaxResponseModel CalculateSalesTax(CalculateSalesTaxRequestModel requestModel)
        {
            return new CalculateSalesTaxResponseModel
            {
                TaxResponseModel = new TaxResponseModel
                {
                    AmountToCollect = requestModel.Amount * 0.09,
                    OrderTotalAmount = requestModel.Amount + requestModel.Shipping,
                    Rate = 0.09,
                    TaxableAmount = requestModel.Amount
                }
            };
        }

        public GetTaxRatesResponseModel GetTaxRates(GetTaxRatesRequestModel requestModel)
        {
            return new GetTaxRatesResponseModel
            {
                RateResponseModel = new RateResponseModel
                {
                    CombinedRate = 0.09,
                    CountryRate = 0.02,
                    CountyRate = 0.01,
                    StateRate = 0.06
                }
            };
        }
    }
}
