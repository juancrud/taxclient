﻿using System;
using TaxClient.Enums;

namespace TaxClient.TaxCalculators
{
    public class TaxCalculatorFactory : ITaxCalculatorFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public TaxCalculatorFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public ITaxCalculator GetTaxCalculator(TaxCalculatorType taxCalculator)
        {
            switch (taxCalculator)
            {
                case TaxCalculatorType.Flat:
                    return (ITaxCalculator) _serviceProvider.GetService(typeof(FlatTaxCalculator));
                case TaxCalculatorType.TaxJar:
                    return (ITaxCalculator) _serviceProvider.GetService(typeof(TaxJarCalculator));
                default:
                    throw new ArgumentOutOfRangeException(nameof(taxCalculator));
            }
        }
    }
}
