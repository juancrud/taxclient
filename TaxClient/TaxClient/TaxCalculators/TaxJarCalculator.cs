﻿using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using TaxClient.Extensions;
using TaxClient.ResourceModels;
using TaxClient.RestApi;

namespace TaxClient.TaxCalculators
{
    public class TaxJarCalculator : ITaxCalculator
    {
        private readonly IRestApiClient _apiClient;
        private readonly IConfiguration _configuration;

        public TaxJarCalculator(IRestApiClient apiClient, IConfiguration configuration)
        {
            _apiClient = apiClient;
            _configuration = configuration;
        }

        public CalculateSalesTaxResponseModel CalculateSalesTax(CalculateSalesTaxRequestModel requestModel)
        {
            var accessToken = _configuration["TaxJar-Api-AccessToken"];
            var baseUrl = _configuration["TaxJar-Api-BaseUrl"];
            return Task.Run(() => _apiClient.PostAsync<CalculateSalesTaxResponseModel, CalculateSalesTaxRequestModel>($"{baseUrl}taxes", requestModel, accessToken)).Result;
        }

        public GetTaxRatesResponseModel GetTaxRates(GetTaxRatesRequestModel requestModel)
        {
            var accessToken = _configuration["TaxJar-Api-AccessToken"];
            var baseUrl = _configuration["TaxJar-Api-BaseUrl"];
            var queryString = requestModel.BuildQueryString();
            return Task.Run(() => _apiClient.GetAsync<GetTaxRatesResponseModel>($"{baseUrl}rates/{requestModel.Zip}?{queryString}", accessToken)).Result;
        }
    }
}
