#pragma checksum "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "0f99fde30aef0195ed9db2fa8eba57510ca00e72"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Home_GetTaxRatesForm), @"mvc.1.0.view", @"/Views/Home/GetTaxRatesForm.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\_ViewImports.cshtml"
using TaxClient;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\_ViewImports.cshtml"
using TaxClient.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
using TaxClient.Enums;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"0f99fde30aef0195ed9db2fa8eba57510ca00e72", @"/Views/Home/GetTaxRatesForm.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"edafe4b7b3e1b91e2e8b80dae2accacd6ca47649", @"/Views/_ViewImports.cshtml")]
    public class Views_Home_GetTaxRatesForm : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<GetTaxRatesRequestViewModel>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 4 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
  
    var enumList = Enum.GetValues(typeof(TaxCalculatorType)).Cast<TaxCalculatorType>()
        .Select(x => new SelectListItem(x.ToString(), x.ToString()));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n<div class=\"card\">\r\n    <div class=\"card-body\">\r\n");
#nullable restore
#line 11 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
         using (Html.BeginForm("GetTaxRates", "Home", FormMethod.Post, new { id = "GetTaxRatesForm" }))
        {

#line default
#line hidden
#nullable disable
            WriteLiteral("            <h3>GetRates method</h3>\r\n            <div class=\"row\">\r\n                ");
#nullable restore
#line 15 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
           Write(Html.LabelFor(x => x.TaxCalculatorType, new { @class = "col-sm-2 col-form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                <div class=\"col-sm-10\">\r\n                    ");
#nullable restore
#line 17 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.DropDownListFor(x => x.TaxCalculatorType, enumList, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 18 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.ValidationMessageFor(x => x.TaxCalculatorType));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                ");
#nullable restore
#line 22 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
           Write(Html.LabelFor(x => x.Street, new { @class = "col-sm-2 col-form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                <div class=\"col-sm-10\">\r\n                    ");
#nullable restore
#line 24 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.TextBoxFor(x => x.Street, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 25 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.ValidationMessageFor(x => x.Street));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                ");
#nullable restore
#line 29 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
           Write(Html.LabelFor(x => x.City, new { @class = "col-sm-2 col-form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                <div class=\"col-sm-10\">\r\n                    ");
#nullable restore
#line 31 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.TextBoxFor(x => x.City, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 32 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.ValidationMessageFor(x => x.City));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                ");
#nullable restore
#line 36 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
           Write(Html.LabelFor(x => x.Zip, new { @class = "col-sm-2 col-form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                <div class=\"col-sm-10\">\r\n                    ");
#nullable restore
#line 38 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.TextBoxFor(x => x.Zip, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 39 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.ValidationMessageFor(x => x.Zip));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                ");
#nullable restore
#line 43 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
           Write(Html.LabelFor(x => x.County, new { @class = "col-sm-2 col-form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                <div class=\"col-sm-10\">\r\n                    ");
#nullable restore
#line 45 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.TextBoxFor(x => x.County, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 46 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.ValidationMessageFor(x => x.County));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n            <div class=\"row\">\r\n                ");
#nullable restore
#line 50 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
           Write(Html.LabelFor(x => x.State, new { @class = "col-sm-2 col-form-label" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                <div class=\"col-sm-10\">\r\n                    ");
#nullable restore
#line 52 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.TextBoxFor(x => x.State, new { @class = "form-control" }));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                    ");
#nullable restore
#line 53 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
               Write(Html.ValidationMessageFor(x => x.State));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </div>\r\n            </div>\r\n            <button id=\"GetTaxRatesButton\" type=\"button\" class=\"btn btn-primary\">Get Tax Rates</button>\r\n");
#nullable restore
#line 57 "C:\Users\juan.rudin\Documents\Code\taxclient\TaxClient\TaxClient\Views\Home\GetTaxRatesForm.cshtml"
        }

#line default
#line hidden
#nullable disable
            WriteLiteral("        <div id=\"GetTaxRatesResponse\" class=\"card card-body\"></div>\r\n    </div>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<GetTaxRatesRequestViewModel> Html { get; private set; }
    }
}
#pragma warning restore 1591
