﻿using TaxClient.Enums;
using TaxClient.ResourceModels;
using TaxClient.TaxCalculators;

namespace TaxClient.Services
{
    public class TaxService : ITaxService
    {
        private readonly ITaxCalculatorFactory _taxCalculatorFactory;

        public TaxService(ITaxCalculatorFactory taxCalculatorFactory)
        {
            _taxCalculatorFactory = taxCalculatorFactory;
        }

        public CalculateSalesTaxResponseModel CalculateSalesTaxes(CalculateSalesTaxRequestModel model, TaxCalculatorType taxCalculatorType)
        {
            
            var taxCalculator = _taxCalculatorFactory.GetTaxCalculator(taxCalculatorType);
            return taxCalculator.CalculateSalesTax(model);
        }

        public GetTaxRatesResponseModel GetTaxRates(GetTaxRatesRequestModel model, TaxCalculatorType taxCalculatorType)
        {
            var taxCalculator = _taxCalculatorFactory.GetTaxCalculator(taxCalculatorType);
            return taxCalculator.GetTaxRates(model);
        }
    }
}
