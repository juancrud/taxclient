﻿using TaxClient.Enums;
using TaxClient.ResourceModels;

namespace TaxClient.Services
{
    public interface ITaxService
    {
        CalculateSalesTaxResponseModel CalculateSalesTaxes(CalculateSalesTaxRequestModel model, TaxCalculatorType taxCalculatorType);
        GetTaxRatesResponseModel GetTaxRates(GetTaxRatesRequestModel model, TaxCalculatorType taxCalculatorType);
    }
}
