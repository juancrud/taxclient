﻿var getTaxRates = function () {
    var form = $("#GetTaxRatesForm");
    if (!form.valid())
        return;

    $.ajax({
        data: form.serialize(),
        url: form.attr("action")
    }).done(function(response) {
        $("#GetTaxRatesResponse").html("<pre>" + response + "</pre>");
    });
};

var calculateTaxes = function () {
    var form = $("#CalculateTaxesForm");
    if (!form.valid())
        return;

    $.ajax({
        data: form.serialize(),
        url: form.attr("action")
    }).done(function (response) {
        $("#CalculateTaxesResponse").html("<pre>" + response + "</pre>");
    });
};

$("#GetTaxRatesButton").on("click", getTaxRates);
$("#CalculateTaxes").on("click", calculateTaxes);
