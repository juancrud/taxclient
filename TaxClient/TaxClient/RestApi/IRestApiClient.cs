﻿using System.Threading.Tasks;

namespace TaxClient.RestApi
{
    public interface IRestApiClient
    {
        Task<TResponse> GetAsync<TResponse>(string url, string accessToken = null);
        Task<TResponse> PostAsync<TResponse, TRequest>(string url, TRequest requestModel, string accessToken = null);
    }
}
