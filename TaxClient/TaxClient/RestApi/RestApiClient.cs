﻿using Newtonsoft.Json;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace TaxClient.RestApi
{
    public class RestApiClient : IRestApiClient
    {
        private readonly HttpClient _httpClient;

        public RestApiClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<TResponse> GetAsync<TResponse>(string url, string accessToken = null)
        {
            var responseString = await PerformRequest(HttpMethod.Get, url, null, accessToken);
            return JsonConvert.DeserializeObject<TResponse>(responseString);
        }

        public async Task<TResponse> PostAsync<TResponse, TRequest>(string url, TRequest requestModel, string accessToken = null)
        {
            var requestString = JsonConvert.SerializeObject(requestModel);
            var responseString = await PerformRequest(HttpMethod.Post, url, requestString, accessToken);
            return JsonConvert.DeserializeObject<TResponse>(responseString);
        }

        private async Task<string> PerformRequest(HttpMethod httpMethod, string url, string body = null, string accessToken = null)
        {
            string responseContent;

            using (var requestMessage = new HttpRequestMessage(httpMethod, url))
            {
                if (!string.IsNullOrEmpty(accessToken))
                {
                    requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                }

                if (body != null)
                {
                    HttpContent httpContent = new StringContent(body, Encoding.UTF8, "application/json");
                    requestMessage.Content = httpContent;
                }

                using (var response = await _httpClient.SendAsync(requestMessage))
                {
                    if (response.StatusCode == HttpStatusCode.Unauthorized) { throw new Exception("HttpUnauthorized"); }
                    responseContent = await response.Content.ReadAsStringAsync();
                }
            }

            return responseContent;
        }
    }
}
